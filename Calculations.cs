﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC_Formule
{
    class Calculations
    {
        protected double A;
        protected double B;
        protected double C;
        protected double DiscriminantValue;

        public Calculations (double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
        }

        public double Discriminant ()
        {
            DiscriminantValue = B*B-4*A*C;
            return DiscriminantValue;
        }

        public string ValidDiscriminant()
        {
            if (DiscriminantValue < 0) //Geen uitkomst
            {
                return "Kleiner";
            }
            else if (DiscriminantValue == 0) //1 uitkomst
            {
                return "Gelijk";
            }
            else if (DiscriminantValue > 0) //Twee uitkomsten
            {
                return "Groter";
            }
            else //Bij fout
            {
                return "Error";
            }

        }

        public double OutputOnlyX ()
        {
            return (-B + Math.Sqrt(DiscriminantValue) ) / (2 * A);
        }

        public double OutputX1 ()
        {
            return (-B - Math.Sqrt(DiscriminantValue)) / (2 * A);
        }

        public double OutputX2 ()
        {
            return (-B + Math.Sqrt(DiscriminantValue)) / (2 * A);
        }
    }
}
