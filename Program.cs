﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABC_Formule
{
    class Program
    {
        static void Main(string[] args)
        {
            bool keepGoing = true;
            while (keepGoing)
            {
                //Definieren strings gebruikt in Main
                string a;
                string b;
                string c;
                string keepGoingString;
                string discriminantString;

                double aDouble;
                double bDouble;
                double cDouble;

                double discriminantValue;
                double singleX;
                double doubleX1;
                double doubleX2;

                //Vraag de user om input
                Console.WriteLine("Welkom bij de ABC-Calculator. Gemaakt door Luciano Nooijen.");
                Console.WriteLine("Zorg voor een correcte invoer. De opbouw van een formule is ax^2 + bx + c.");
                Console.WriteLine("");
                Console.WriteLine("Vul de juiste variabelen in: (komma = ,)");
                Console.Write("a: "); a = Console.ReadLine();
                Console.Write("b: "); b = Console.ReadLine();
                Console.Write("b: "); c = Console.ReadLine();

                //Toevoegen input validatie en . naar ,

                //Omzetten van string naar double 
                aDouble = Convert.ToDouble(a);
                bDouble = Convert.ToDouble(b);
                cDouble = Convert.ToDouble(c);
                               
                Console.WriteLine("a, b en c zijn: " + aDouble + ", " + bDouble + " en " + cDouble);
                Console.WriteLine("");

                //Invoeren getallen in berekenfunctie
                Calculations berekeningen = new Calculations(aDouble, bDouble, cDouble);

                //Discriminant berekenen
                discriminantValue = berekeningen.Discriminant();

                //Validatie discriminant en output
                discriminantString = berekeningen.ValidDiscriminant();
                if (discriminantString == "Gelijk") //Enkele output 
                {
                    singleX = berekeningen.OutputOnlyX();
                    Console.WriteLine("De discriminant is: " + discriminantValue);
                    Console.WriteLine("Het x-coördinaat is: " + singleX);
                }
                else if (discriminantString == "Groter") //Dubbele output
                {
                    doubleX1 = berekeningen.OutputX1();
                    doubleX2= berekeningen.OutputX2();
                    Console.WriteLine("De discriminant is: " + discriminantValue);
                    Console.WriteLine("Het x1-coördinaat is: " + doubleX1); //Toevoegen rounding
                    Console.WriteLine("Het x2-coördinaat is: " + doubleX2); //Toevoegen rounding
                }
                else if (discriminantString == "Kleiner") //Geen snijpunt
                {
                    Console.WriteLine("De discriminant is: " + discriminantValue + " en er is dus geen snijpunt met de x-as.");
                }
                else //Ongeldige invoer
                {
                    Console.WriteLine("Ongeldige invoer");
                }

                //Vragen of gebruiker door wil gaan
                Console.Write("Druk op enter om door te gaan, of druk op typ q om af te sluiten: "); keepGoingString = Console.ReadLine();
                if (keepGoingString.Equals("q"))
                {
                    keepGoing = false;
                }
                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("-----------------------------------------------------------");
                    Console.WriteLine("");
                    Console.Clear();
                }              
            }

        }
    }
}
